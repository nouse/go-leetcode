package leetcode

import "testing"

func TestTrap(t *testing.T) {
	height := []int{0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1}
	if trap(height) != 6 {
		t.Errorf("unexpected %d", trap(height))
	}
	height = []int{2, 1, 0, 2}
	if trap(height) != 3 {
		t.Errorf("unexpected %d", trap(height))
	}
	height = []int{0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1}
	if trap(height) != 6 {
		t.Errorf("unexpected %d", trap(height))
	}
	height = []int{5, 2, 1, 2, 1, 5}
	if trap(height) != 14 {
		t.Errorf("unexpected %d", trap(height))
	}
}
