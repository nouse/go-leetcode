package leetcode

import (
	"testing"
)

func TestFourSum(t *testing.T) {
	ints := []int{3, 0, 1, -2, -1, 4}
	target := 6

	expected := []quadruple{
		{-2, 1, 3, 4},
		{-1, 0, 3, 4},
	}
	t.Run("6: 3 0 1 -2 -1 4 ", verifyFourSums(ints, target, expected))

	ints = []int{0, 0, 0, 0}
	target = 0
	expected = []quadruple{{0, 0, 0, 0}}

	t.Run("0: 0 0 0 0", verifyFourSums(ints, target, expected))

	ints = []int{1, 1, 2, 2, 0, 1}
	target = 5
	expected = []quadruple{{0, 1, 2, 2}, {1, 1, 1, 2}}
	t.Run("5: 1 1 2 2 0 1", verifyFourSums(ints, target, expected))

	ints = []int{-3, -2, -1, 0, 0, 1, 2, 3}
	target = 0
	expected = []quadruple{{-3, -2, 2, 3}, {-3, -1, 1, 3}, {-3, 0, 0, 3}, {-3, 0, 1, 2}, {-2, -1, 0, 3}, {-2, -1, 1, 2}, {-2, 0, 0, 2}, {-1, 0, 0, 1}}
	t.Run("0: -3 -2 -1 0 0 1 2 3", verifyFourSums(ints, target, expected))

}

func verifyFourSums(ints []int, target int, expected []quadruple) func(*testing.T) {

	return func(t *testing.T) {
		result := fourSum(ints, target)
		if len(result) != len(expected) {
			t.Errorf("Expected to return %d results, actual %d", len(expected), len(result))
		}

		total := len(result)
		if total > len(expected) {
			total = len(expected)
		}
		for i := 0; i < total; i++ {
			current := result[i]
			expectedC := expected[i]
			for j, v := range current {
				if v != expectedC[j] {
					t.Errorf("Expected of result %d is %v, but is %v", i, expectedC, current)
					break
				}
			}
		}
	}
}
