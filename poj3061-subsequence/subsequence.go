package poj3061

// Find the minimal length of the subsequence of consecutive elements of the sequence,
// the sum of which is greater than or equal to S.
func subsequence(sum int, items []int) int {
	var (
		i       = 0
		j       = 1
		minimal = len(items)
		s       = items[i]
	)
	for j < len(items) {
		for (s < sum) && (j < len(items)) {
			s += items[j]
			j++
		}
		if minimal > (j - i) {
			minimal = j - i
		}
		i++
		s -= items[i]
	}
	return minimal
}
