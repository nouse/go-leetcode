package poj3061

import "testing"

func TestSubsequence(t *testing.T) {

	sum := 15
	items := []int{5, 1, 3, 5, 10, 7, 4, 9, 2, 8}

	if subsequence(sum, items) != 2 {
		t.Errorf("Expected return 2, actual: %d", subsequence(sum, items))
	}
}
