package q212

import (
	"sort"
	"testing"
)

func TestWordSearch(t *testing.T) {
	words := []string{"oath", "pea", "eat", "rain"}

	board := [][]byte{
		{'o', 'a', 'a', 'n'},
		{'e', 't', 'a', 'e'},
		{'i', 'h', 'k', 'r'},
		{'i', 'f', 'l', 'v'},
	}

	result := findWords(board, words)
	sort.Strings(result)

	if len(result) != 2 || result[0] != "eat" || result[1] != "oath" {
		t.Errorf("Expected findWords to return {\"eat\", \"oath\"}, actual: %v", result)
	}
}
