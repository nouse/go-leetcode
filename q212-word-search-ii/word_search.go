package q212

type Trie struct {
	root *Node
}

func (trie *Trie) Add(word string) {
	root := trie.root
	for i, b := range word {
		index := b - 'a'

		if root.children[index] == nil {
			root.children[index] = &Node{
				s: word[:i+1],
			}
		}
		root = root.children[index]
	}
	root.complete = true
}

type Node struct {
	children [26]*Node
	s        string
	complete bool
}

// Env represents what will not change
type Env struct {
	board         [][]byte
	rows, columns int
	trie          *Trie
}

// Status represents what will change
type Status struct {
	i, j      int
	searched  [][]bool
	solutions []string
	node      *Node
	Env
}

func MakeEnv(board [][]byte, words []string) Env {
	trie := &Trie{root: &Node{}}
	for _, word := range words {
		trie.Add(word)
	}

	rows := len(board)
	columns := len(board[0])

	return Env{
		board:   board,
		rows:    rows,
		columns: columns,
		trie:    trie,
	}
}

type Choice struct {
	i, j int
	node *Node
}

var neighbours = [4][2]int{
	{-1, 0},
	{0, 1},
	{1, 0},
	{0, -1},
}

func (st *Status) Choices() (choices []Choice) {
	for _, d := range neighbours {
		x, y := st.i+d[0], st.j+d[1]
		if x < 0 || y < 0 || x >= st.rows || y >= st.columns || st.searched[x][y] {
			continue
		}
		b := st.board[x][y]
		child := st.node.children[b-'a']
		if child == nil {
			continue
		}
		if child.complete {
			st.solutions = append(st.solutions, child.s)
		}

		choices = append(choices, Choice{
			i:    x,
			j:    y,
			node: child,
		})
	}
	return
}

func (st *Status) FindWords(choices []Choice) {
	if len(choices) == 0 {
		return
	}
	current := Choice{
		i:    st.i,
		j:    st.j,
		node: st.node,
	}
	st.searched[st.i][st.j] = true
	defer func() {
		st.i = current.i
		st.j = current.j
		st.node = current.node
		st.searched[st.i][st.j] = false
	}()

	for _, choice := range choices {
		st.i = choice.i
		st.j = choice.j
		st.node = choice.node
		nextChoices := st.Choices()
		st.FindWords(nextChoices)
	}
}

func findWords(board [][]byte, words []string) (result []string) {
	env := MakeEnv(board, words)

	searched := make([][]bool, env.rows)
	for i := 0; i < env.rows; i++ {
		searched[i] = make([]bool, env.columns)
	}
	st := &Status{
		searched: searched,
		Env:      env,
	}
	for i, row := range board {
		for j, b := range row {
			child := env.trie.root.children[b-'a']
			if child == nil {
				continue
			}
			st.i = i
			st.j = j
			st.node = env.trie.root
			st.searched[i][j] = true
			choices := st.Choices()
			st.FindWords(choices)
			st.searched[i][j] = false
		}
	}
	return st.solutions
}
