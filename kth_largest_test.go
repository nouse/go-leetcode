package leetcode

import "testing"

func TestFindKthLargest(t *testing.T) {
	result := findKthLargest([]int{3, 2, 1, 5, 6, 4}, 2)
	if result != 5 {
		t.Errorf("Unexpected %d", result)
	}
}
