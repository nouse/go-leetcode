package q632

import "testing"

func TestSmallestRange(t *testing.T) {
	nums := [][]int{
		{4, 10, 15, 24, 26},
		{0, 9, 12, 20},
		{5, 18, 22, 30},
	}

	r := smallestRange(nums)
	if r[0] != 20 || r[1] != 24 {
		t.Errorf("Expected small range is [20,24], actual: [%d,%d]", r[0], r[1])
	}

	nums = [][]int{
		{10, 10},
		{11, 11},
	}
	r = smallestRange(nums)
	if r[0] != 10 || r[1] != 11 {
		t.Errorf("Expected small range is [10,11], actual: [%d,%d]", r[0], r[1])
	}
}
