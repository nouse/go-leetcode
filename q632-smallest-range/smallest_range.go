package q632

import (
	"sort"
)

type Range struct {
	Min int
	Max int
	Sub int
}

type Item struct {
	Column int
	Row    int
	Value  int
}

func smallestRange(nums [][]int) []int {
	columns := len(nums)
	items := make([]Item, columns)
	for i := 0; i < columns; i++ {
		items[i] = Item{
			Column: i,
			Value:  nums[i][0],
		}
	}

	sort.Slice(items, func(i, j int) bool { return items[i].Value < items[j].Value })
	r := Range{
		Min: items[0].Value,
		Max: items[columns-1].Value,
	}
	r.Sub = r.Max - r.Min

	for {
		lowest := items[0]
		if lowest.Row == len(nums[lowest.Column])-1 {
			break
		}
		value := nums[lowest.Column][lowest.Row+1]
		index := sort.Search(columns, func(i int) bool { return items[i].Value >= value })
		if index == 0 {
			items[0].Row++
			continue
		}
		copy(items[0:index-1], items[1:index])

		items[index-1] = Item{
			Column: lowest.Column,
			Row:    lowest.Row + 1,
			Value:  value,
		}
		if items[columns-1].Value-items[0].Value < r.Sub {
			r.Max = items[columns-1].Value
			r.Min = items[0].Value
			r.Sub = r.Max - r.Min
		}
	}
	return []int{r.Min, r.Max}
}
