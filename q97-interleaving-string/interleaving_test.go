package q97

import "testing"

func TestInterleave(t *testing.T) {

	s1 := "aabcc"
	s2 := "dbbca"
	s3 := "aadbbcbcac"

	if !isInterleave(s1, s2, s3) {
		t.Errorf("Expect isInterleave of %s, %s, %s is true, actual false", s1, s2, s3)
	}
}
