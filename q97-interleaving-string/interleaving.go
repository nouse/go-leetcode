package q97

func isInterleave(s1 string, s2 string, s3 string) bool {
	var (
		m = len(s1)
		n = len(s2)
	)
	if m+n != len(s3) {
		return false
	}
	if m == 0 {
		return s2 == s3
	}
	if n == 0 {
		return s1 == s3
	}

	// Initialize dp table
	dp := make([][]bool, m+1)
	for i := 0; i <= m; i++ {
		dp[i] = make([]bool, n+1)
	}

	// Fill first column
	dp[0][0] = true
	for j := 1; j <= n; j++ {
		if s3[j-1] != s2[j-1] {
			break
		}
		dp[0][j] = true
	}

	for i := 1; i <= m; i++ {
		if dp[i-1][0] && s3[i-1] == s1[i-1] {
			dp[i][0] = true
		}
		for j := 1; j <= n; j++ {
			switch {
			case dp[i-1][j] && s3[i+j-1] == s1[i-1]:
				dp[i][j] = true
			case dp[i][j-1] && s3[i+j-1] == s2[j-1]:
				dp[i][j] = true
			case !dp[i-1][j]:
				continue
			}
		}
	}

	return dp[m][n]
}
