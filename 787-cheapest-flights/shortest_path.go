package main

import (
	"fmt"
)

func main() {
	paths := [][3]int{
		{4, 5, 35},
		{5, 4, 35},
		{4, 7, 37},
		{5, 7, 28},
		{7, 5, 28},
		{5, 1, 32},
		{0, 4, 38},
		{0, 2, 26},
		{7, 3, 39},
		{1, 3, 29},
		{2, 7, 34},
		{6, 2, 40},
		{3, 6, 52},
		{6, 0, 58},
		{6, 4, 93},
	}
	nodes := 8
	pathArray := make([][][2]int, nodes)
	for _, path := range paths {
		src, dst, weight := path[0], path[1], path[2]
		pathArray[src] = append(pathArray[src], [2]int{dst, weight})
	}

	sp := make([]int, nodes)
	src := 0
	for _, d := range pathArray[src] {
		sp[d[0]] = d[1]
		fillSP(sp, pathArray, d[0])
	}
	sp[src] = 0
	fmt.Printf("%v\n", sp)
}

func fillSP(sp []int, pathArray [][][2]int, src int) {
	for _, d := range pathArray[src] {
		dst, weight := d[0], d[1]
		target := sp[src] + weight
		if sp[dst] == 0 || sp[dst] > target {
			sp[dst] = sp[src] + weight
			fillSP(sp, pathArray, dst)
		}
	}
}
