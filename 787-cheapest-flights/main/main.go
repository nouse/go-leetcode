package main

import "fmt"

func findCheapestPrice(n int, flights [][]int, src int, dst int, K int) int {
	flightArray := make([][][2]int, n)
	for _, flight := range flights {
		s, d, w := flight[0], flight[1], flight[2]
		flightArray[s] = append(flightArray[s], [2]int{d, w})
	}

	// sp[i] will be a K length slice
	sp := make([][]int, n)
	for i := 0; i < n; i++ {
		sp[i] = make([]int, K+1)
	}
	for _, f := range flightArray[src] {
		sp[f[0]][0] = f[1]
		fillSP(sp, flightArray, f[0], 0, K)
	}

	r := -1
	for _, v := range sp[dst] {
		if v != 0 {
			if r == -1 {
				r = v
			} else if v < r {
				r = v
			}
		}
	}
	return r
}

func fillSP(sp [][]int, flightArray [][][2]int, src, index, K int) {
	if index >= K {
		return
	}
	for _, d := range flightArray[src] {
		dst, weight := d[0], d[1]
		target := sp[src][index] + weight
		if sp[dst][index+1] == 0 || sp[dst][index+1] > target {
			sp[dst][index+1] = target
			fillSP(sp, flightArray, dst, index+1, K)
		}
	}
}

func main() {
	flights := [][]int{
		{0, 1, 100},
		{1, 2, 100},
		{0, 2, 500},
	}
	fmt.Printf("%d\n", findCheapestPrice(3, flights, 0, 2, 0))
}
