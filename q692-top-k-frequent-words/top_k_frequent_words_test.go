package q692

import "testing"

func TestTopKFrequent(t *testing.T) {
	words := []string{"i", "love", "leetcode", "i", "love", "coding"}
	top := topKFrequent(words, 2)
	if len(top) != 2 || top[0] != "i" || top[1] != "love" {
		t.Errorf("Expected to return [\"i\", \"love\"], actual: %v", top)
	}
}
