package q692

import (
	"sort"
)

func topKFrequent(words []string, k int) []string {
	d := map[string]int{}
	for _, w := range words {
		d[w]++
	}
	l := make([]string, len(d))
	index := 0
	for k := range d {
		l[index] = k
		index++
	}

	sort.Slice(l, func(i, j int) bool {
		w1 := l[i]
		w2 := l[j]
		if d[w1] < d[w2] {
			return false
		}
		if d[w1] > d[w2] {
			return true
		}
		return w1 < w2
	})
	return l[0:k]
}
