package leetcode

func trap(height []int) int {
	if len(height) == 0 {
		return 0
	}
	var shadowLTR, shadowRTL, totalMass, maxLTR, maxRTL int
	for i, h := range height {
		if h > maxLTR {
			maxLTR = h
		}
		shadowLTR += maxLTR
		if height[len(height)-1-i] > maxRTL {
			maxRTL = height[len(height)-1-i]
		}
		shadowRTL += maxRTL
		totalMass += h
	}
	return shadowLTR + shadowRTL - maxLTR*len(height) - totalMass
}
