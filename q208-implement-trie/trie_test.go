package q208

import "testing"

func TestTrie(t *testing.T) {
	trie := Constructor()

	trie.Insert("apple")
	trie.Search("apple")
	trie.Search("app")
	trie.StartsWith("app")
	trie.Insert("app")
	trie.Search("app")
}
