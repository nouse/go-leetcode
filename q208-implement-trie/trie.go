package q208

type Trie struct {
	Children map[byte]*Trie
	Complete bool
}

/** Initialize your data structure here. */
func Constructor() Trie {
	return Trie{
		Children: make(map[byte]*Trie),
	}
}

/** Inserts a word into the trie. */
func (this *Trie) Insert(word string) {
	child, exists := this.Children[word[0]]
	if !exists {
		t := Constructor()
		child = &t
		this.Children[word[0]] = child
	}
	if len(word) == 1 {
		child.Complete = true
		return
	}
	child.Insert(word[1:])
}

/** Returns if the word is in the trie. */
func (this *Trie) Search(word string) bool {
	child, exists := this.Children[word[0]]
	if !exists {
		return false
	}
	if len(word) == 1 {
		return child.Complete
	}
	return child.Search(word[1:])
}

/** Returns if there is any word in the trie that starts with the given prefix. */
func (this *Trie) StartsWith(prefix string) bool {
	child, exists := this.Children[prefix[0]]
	if !exists {
		return false
	}
	if len(prefix) == 1 {
		return true
	}
	return child.StartsWith(prefix[1:])
}
