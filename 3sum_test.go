package leetcode

import (
	"bufio"
	"os"
	"strconv"
	"testing"
)

func BenchmarkThreeSum(B *testing.B) {
	ints := []int{}
	var i int
	var err error
	f, err := os.Open("testdata/1Kints.txt")
	if err != nil {
		B.Fatal(err)
	}
	b := bufio.NewScanner(f)
	b.Split(bufio.ScanLines)
	for b.Scan() {
		i, err = strconv.Atoi(b.Text())
		if err != nil {
			continue
		}
		ints = append(ints, i)
	}
	err = f.Close()
	if err != nil {
		B.Fatal(err)
	}
	for i := 0; i < B.N; i++ {
		B.StopTimer()
		innerInts := make([]int, len(ints))
		copy(innerInts, ints)
		B.StartTimer()
		threeSum(innerInts)
	}
}

func TestThreeSum(t *testing.T) {
	ints := []int{-1, 0, 1, 2, -1, -4}

	expected := []triple{
		{-1, -1, 2},
		{-1, 0, 1},
	}
	t.Run("-1 0 1 2 -1 -4", verifyThreeSums(ints, expected))

	ints = []int{0, 0, 0}
	expected = []triple{{0, 0, 0}}

	t.Run("0 0 0", verifyThreeSums(ints, expected))

	ints = []int{-1, 1, 0, 1, 0}
	expected = []triple{{-1, 0, 1}}

	t.Run("-1 0 1", verifyThreeSums(ints, expected))

	ints = []int{3, 0, -2, -1, 1, 2}
	expected = []triple{{-2, -1, 3}, {-2, 0, 2}, {-1, 0, 1}}
	t.Run("3 0 -2 -1 1 2", verifyThreeSums(ints, expected))
}

func verifyThreeSums(ints []int, expected []triple) func(*testing.T) {

	return func(t *testing.T) {
		result := threeSum(ints)
		if len(result) != len(expected) {
			t.Errorf("Expected to return %d results, actual %d", len(expected), len(result))
		}

		total := len(result)
		if total > len(expected) {
			total = len(expected)
		}
		for i := 0; i < total; i++ {
			current := result[i]
			expectedC := expected[i]
			if current[0] != expectedC[0] || current[1] != expectedC[1] || current[2] != expectedC[2] {
				t.Errorf("Expected of result %d is %v, but is %v", i, expectedC, current)
			}
		}
	}
}
