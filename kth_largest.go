package leetcode

import (
	"sort"
)

func findKthLargest(nums []int, k int) int {
	result := make([]int, k)
	copy(result, nums)
	sort.Ints(result)
	for i := k; i < len(nums); i++ {
		n := nums[i]
		if n <= result[0] {
			continue
		}
		index := sort.SearchInts(result, n)
		if index == 1 {
			nums[0] = n
			continue
		}
		copy(result[0:index-1], result[1:index])
		result[index-1] = n
	}
	return result[0]
}
