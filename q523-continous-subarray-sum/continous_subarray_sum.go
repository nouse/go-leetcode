package q523

func checkSubarraySum(nums []int, k int) bool {
	if len(nums) < 2 {
		return false
	}
	if k == 0 {
		for i := 0; i < len(nums)-1; i++ {
			if (nums[i] == 0) && (nums[i+1] == 0) {
				return true
			}
		}
		return false
	}

	remains := make([]int, len(nums))
	remains[0] = nums[0] % k

	for i := 1; i < len(nums); i++ {
		currentRemain := nums[i] % k
		for j := 0; j < i; j++ {
			remains[j] = (remains[j] + currentRemain) % k
			if remains[j] == 0 {
				return true
			}
		}
		remains[i] = currentRemain
	}
	return false
}
