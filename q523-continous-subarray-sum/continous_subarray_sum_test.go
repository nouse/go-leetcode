package q523

import (
	"fmt"
	"testing"
)

type testCase struct {
	nums     []int
	k        int
	expected bool
}

func TestCheckSubarraySum(t *testing.T) {
	testCases := []testCase{
		{
			nums:     []int{0, 0},
			k:        -1,
			expected: true,
		},
		{
			nums:     []int{23, 2, 4, 3, 7},
			k:        6,
			expected: true,
		},
		{
			nums:     []int{23, 2, 6, 3, 7},
			k:        6,
			expected: true,
		},
	}
	for _, tc := range testCases {
		t.Run(fmt.Sprintf("%v,%d", tc.nums, tc.k), func(t *testing.T) {
			result := checkSubarraySum(tc.nums, tc.k)
			if result != tc.expected {
				t.Errorf("Expected to return %t, actual %t", tc.expected, result)
			}
		})
	}
}
