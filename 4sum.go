package leetcode

import (
	"sort"
)

type quadruple [4]int

// fourSums will modify nums, be care!
func fourSum(nums []int, target int) (result []quadruple) {

	numsLen := len(nums)
	if numsLen < 3 {
		return
	}
	sort.Ints(nums)

	var k, l, sum, currentI, currentJ, expected, innerTarget int
	for i := 0; i < numsLen-2; i++ {
		if i != 0 && nums[i] == currentI {
			continue
		}
		currentI = nums[i]
		for j := numsLen - 1; j > i; j-- {
			if j != numsLen-1 && nums[j] == currentJ {
				continue
			}
			currentJ = nums[j]
			l = j - 1
			innerTarget = target - currentI - currentJ
			expected = innerTarget - nums[l]
			k = search(i+1, l-1, func(index int) bool { return nums[index] >= expected })

			for k < l {
				sum = nums[k] + nums[l]
				if sum == innerTarget {
					result = append(result, quadruple{nums[i], nums[k], nums[l], nums[j]})
					for k++; k < l && nums[k] == nums[k-1]; k++ {
					}
					for l--; k < l && nums[l] == nums[l+1]; l-- {
					}
				} else if sum > innerTarget {
					for l--; k < l && nums[l] == nums[l+1]; l-- {
					}
				} else {
					for k++; k < l && nums[k] == nums[k-1]; k++ {
					}
				}
			}
		}

	}

	return
}
