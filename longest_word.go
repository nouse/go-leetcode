package leetcode

import (
	"sort"
)

type Bucket = sort.StringSlice

func longestWord(words []string) string {
	if len(words) == 0 {
		return ""
	}
	buckets := [30]Bucket{}

	for _, word := range words {
		index := len(word) - 1
		buckets[index] = append(buckets[index], word)
	}

	for _, bucket := range buckets {
		bucket.Sort()
	}

	for i := 29; i > 0; i-- {
		bucket := buckets[i]
		target := ""
	outer:
		for _, word := range bucket {
			if word[0:i] == target {
				continue
			}
			for l := i; l > 0; l-- {
				target = word[0:l]
				b := buckets[l-1]
				index := b.Search(target)
				if index == -1 || index == len(b) || b[index] != target {
					continue outer
				}
			}
			return word
		}
	}
	return buckets[0][0]
}
