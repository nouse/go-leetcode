package leetcode

import (
	"sort"
)

type triple [3]int

func search(m, n int, f func(int) bool) int {
	// Define f(-1) == false and f(n) == true.
	// Invariant: f(i-1) == false, f(j) == true.
	i, j := m, n
	for i < j {
		h := int(uint(i+j) >> 1) // avoid overflow when computing h
		// i ≤ h < j
		if !f(h) {
			i = h + 1 // preserves f(i-1) == false
		} else {
			j = h // preserves f(j) == true
		}
	}
	// i == j, f(i-1) == false, and f(j) (= f(i)) == true  =>  answer is i.
	return i
}

// threeSums will modify nums, be careful!
func threeSum(nums []int) (result []triple) {

	numsLen := len(nums)
	if numsLen < 3 {
		return
	}
	sort.Ints(nums)

	var j, k, sum, currentI, expected int
	for i := 0; i < numsLen-2; i++ {
		if i != 0 && nums[i] == currentI {
			continue
		}
		currentI = nums[i]
		k = numsLen - 1
		expected = -(currentI + nums[k])
		j = search(i+1, k-1, func(index int) bool { return nums[index] >= expected })

		for j < k {
			sum = nums[i] + nums[j] + nums[k]
			if sum == 0 {
				result = append(result, triple{nums[i], nums[j], nums[k]})
				for j++; j < k && nums[j] == nums[j-1]; j++ {
				}
				for k--; k > j && nums[k] == nums[k+1]; k-- {
				}
			} else if sum > 0 {
				for k--; k > j && nums[k] == nums[k+1]; k-- {
				}
			} else {
				for j++; j < k && nums[j] == nums[j-1]; j++ {
				}
			}
		}
	}

	return
}
