package leetcode

import (
	"math"
	"math/bits"
	"strconv"
)

func smallestGoodBase(n string) string {
	u, _ := strconv.ParseUint(n, 10, 64)
	// We are going to find u in form of 111...111, 11...111,...,111,11
	max := 64 - bits.LeadingZeros64(u)
	for k := max; k > 2; k-- {
		// We know that n^(k-1) < u < (n+1)^(k-1), so n is the nearest number to u^(1/(k-1))
		base := uint64(math.Floor(math.Exp(math.Log(float64(u)) / float64(k-1))))

		if u%base != 1 {
			continue
		}
		// As u upper limit is 2^18, close to 2^64, we must take care of number overflow
		// u = n*(n^(k-2)+n^(k-3)+...+1)+1 => u-1 = n*(n^(k-1)-1)/(n-1)
		if (u-1)/base*(base-1)+1 == exp(base, k-1) {
			return strconv.FormatUint(base, 10)
		}
	}
	// As u can always be represent as u-1 + 1, u-1 is always a good base
	return strconv.FormatUint(u-1, 10)
}

func exp(x uint64, y int) uint64 {
	r := x
	for i := 1; i < y; i++ {
		r *= x
	}
	return r
}
