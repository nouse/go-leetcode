package leetcode

import "testing"

func TestLongestWords(t *testing.T) {
	w := longestWord([]string{"w", "wo", "wor", "worl", "world"})
	if w != "world" {
		t.Errorf("Expected %s", w)
	}
	w = longestWord([]string{"a", "banana", "app", "appl", "ap", "apply", "apple"})
	if w != "apple" {
		t.Errorf("Expected %s", w)
	}
	w = longestWord([]string{"yo", "ew", "fc", "zrc", "yodn", "fcm", "qm", "qmo", "fcmz", "z", "ewq", "yod", "ewqz", "y"})
	if w != "yodn" {
		t.Errorf("Expected %s", w)
	}
}
