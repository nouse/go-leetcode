package q907

import (
	"sort"
)

type node struct {
	value, left int
}

// return sum and new left
func collect(list []node, index int) (int, int) {
	left := 1
	right := 1
	sum := 0
	for i := len(list) - 1; i >= index; i-- {
		last := list[i]
		left += last.left
		sum += last.value * last.left * right
		right += last.left
	}
	return sum, left
}

func sumSubarrayMins(A []int) int {
	if len(A) == 1 {
		return A[0]
	}
	list := []node{{A[0], 1}}
	var sum, num int
	for i := 1; i < len(A); i++ {
		num = A[i]
		if num > list[len(list)-1].value {
			list = append(list, node{num, 1})
			continue
		}

		index := sort.Search(len(list), func(j int) bool {
			return list[j].value >= num
		})
		sum2, left := collect(list, index)
		list = list[0:index]
		sum += sum2
		list = append(list, node{num, left})
	}

	num, _ = collect(list, 0)
	return (sum + num) % 1000000007
}
