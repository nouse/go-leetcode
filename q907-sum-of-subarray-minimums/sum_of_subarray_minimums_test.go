package q907

import (
	"testing"
)

func TestSumOfArrayMinimuns(t *testing.T) {
	if sumSubarrayMins([]int{1}) != 1 {
		t.Fatal("error calculate [1]")
	}
	if sumSubarrayMins([]int{3, 1, 2, 4}) != 17 {
		t.Fatal("error calculate [3,1,2,4]")
	}
	if sumSubarrayMins([]int{62, 92, 97}) != 467 {
		t.Fatal("error calculate [62,92,97]")
	}
	if sumSubarrayMins([]int{23, 80, 34, 82, 11}) != 445 {
		t.Fatal("error calculate [23,80,34,82,11]")
	}
	if sumSubarrayMins([]int{19, 19, 62, 66}) != 323 {
		t.Fatal("error calculate [19,19,62,66]")
	}
	if sumSubarrayMins([]int{23, 80, 34, 25, 82, 11}) != 561 {
		t.Fatal("error calculate [23,80,34,25,82,11]")
	}
}
