package leetcode

import (
	"testing"
)

func TestSmallestGoodBase(t *testing.T) {

	base := smallestGoodBase("13")
	if base != "3" {
		t.Errorf("Unexpected %s of 13", base)
	}
	base = smallestGoodBase("4681")
	if base != "8" {
		t.Errorf("Unexpected %s of 4681", base)
	}
	base = smallestGoodBase("470988884881403701")
	if base != "686286299" {
		t.Errorf("Unexpected %s of 470988884881403701", base)
	}
}

func BenchmarkSmallestGoodBase(B *testing.B) {
	for i := 0; i < B.N; i++ {
		smallestGoodBase("470988884881403701")
	}
}
