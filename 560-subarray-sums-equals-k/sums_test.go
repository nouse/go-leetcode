package sums

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"testing"
)

var (
	nums = []int{}
)

func TestSubArraySum(t *testing.T) {
	r := subarray_sum_array(nums, -682)
	if r != 4012 {
		t.Errorf("Expected to return 4012, actual: %d", r)
	}

	r = subarray_sum_hash(nums, -682)
	if r != 4012 {
		t.Errorf("Expected to return 4012, actual: %d", r)
	}
}

func BenchmarkSubArraySum(b *testing.B) {
	b.Run("array base", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			subarray_sum_array(nums, -682)
		}

	})
	b.Run("hash base", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			subarray_sum_hash(nums, -682)
		}

	})
}

func TestMain(m *testing.M) {
	file, err := os.Open("array.json")
	if err != nil {
		os.Exit(-1)
	}
	b, err := ioutil.ReadAll(file)
	if err != nil {
		os.Exit(-1)
	}
	err = json.Unmarshal(b, &nums)
	if err != nil {
		os.Exit(-1)
	}
	// call flag.Parse() here if TestMain uses flags
	os.Exit(m.Run())
}
