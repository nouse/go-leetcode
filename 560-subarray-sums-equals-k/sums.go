package sums

import "sort"

func subarray_sum_array(nums []int, k int) int {

	sums := make([]int, len(nums))
	sums[0] = nums[0]

	var (
		total  = 0
		sum    = sums[0]
		target = sums[0] - k
	)

	if sum == k {
		total++
	}

	for i := 1; i < len(nums)-1; i++ {
		n := nums[i]
		target += n

		j := sort.Search(i, func(k int) bool { return sums[k] >= target })
		if j < i && sums[j] == target {
			total++
			j++
			for j < i {
				if sums[j] != target {
					break
				}
				total++
				j++
			}
		}

		sum += n
		j = sort.Search(i, func(k int) bool { return sums[k] >= sum })
		if j == i {
			sums[j] = sum
		} else {
			copy(sums[j+1:i+1], sums[j:i])
			sums[j] = sum
		}
	}
	return total
}

func subarray_sum_hash(nums []int, k int) int {
	sums := make(map[int]int, len(nums))

	var (
		total  = 0
		sum    = 0
		target = -k
	)

	for _, n := range nums {
		target += n
		if m := sums[target]; m > 0 {
			total += m
		}

		sum += n
		if sum == k {
			total++
		}
		sums[sum]++
	}
	return total
}
