require 'json'

def subarray_sum(nums, k)
  total = 0
  sums = []
  sum = 0
  target = -k

  nums.each do |n| 
    target += n

    index = sums.bsearch_index{ |s| s >= target }
    if !index.nil? && sums[index] == target
      total += 1
      (index+1..sums.length-1).each do |j| 
        if sums[j] != target
          break
        end
        total += 1
      end
    end

    sum += n
    if sum == k
      total += 1
    end

    index = sums.bsearch_index{ |s| s >= sum }
    if index.nil?
      sums.push(sum)
    else
      sums.insert(index, sum)
    end
  end
  return total
end

nums = JSON.parse(IO.read('array.json'))
puts subarray_sum(nums, -682)
