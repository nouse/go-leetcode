package q85

func maximalRectangle(matrix [][]byte) int {
	max := 0
	rows := len(matrix)
	columns := len(matrix[0])
	if rows == 0 || columns == 0 {
		return max
	}
	searched := make([][]bool, rows)
	for i := 0; i < rows; i++ {
		searched[i] = make([]bool, columns)
	}
	for i := 0; i < rows; i++ {
		for j := 0; j < columns; j++ {
			area := maxArea(matrix, i, j, rows, columns)
			if area > max {
				max = area
			}
		}
	}
	return max
}

func maxArea(matrix [][]byte, i, j, rows, columns int) int {
	area := 0
	lastZero := columns - j
	for x := 0; x < rows-i; x++ {
		y := 0
		for y < lastZero {
			if matrix[i+x][j+y] == '0' {
				lastZero = y
				break
			}
			y++
		}
		current := (x + 1) * y
		if current > area {
			area = current
		}
	}
	return area
}
